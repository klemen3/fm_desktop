package com.kz.fm_desktop;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

public class Charts {

	private JFrame 		chartsFrame;
	private ChartPanel  chartsPanel;
	private PieDataset  dataSet;
	private JFreeChart  chart;
	
	public void drawChartWindow() {
		
		chartsFrame = new JFrame("Chart");
		dataSet = createDataset();
		chart = ChartFactory.createPieChart("Percentages", dataSet, true, true, false);

		PieSectionLabelGenerator labelGenerator = new StandardPieSectionLabelGenerator("{0} : ({2})",
				new DecimalFormat("0"), new DecimalFormat("0%"));
		((PiePlot) chart.getPlot()).setLabelGenerator(labelGenerator);

		// Create Panel
		chartsPanel = new ChartPanel(chart);

		chartsFrame.add(chartsPanel);

		// Frame settings
		chartsFrame.setLocationRelativeTo(null);
		chartsFrame.setVisible(true);
		chartsFrame.setSize(800, 400);
		chartsFrame.pack();
		chartsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	
	
	private PieDataset createDataset() {
		HashMap<String, Double> hmPercentages = Controller.getPercentages(); 
		DefaultPieDataset defaultDataSet = new DefaultPieDataset();
		
		for (Map.Entry<String, Double> entry : hmPercentages.entrySet()) {
			String sKey = entry.getKey();
			double dValue = hmPercentages.get(entry.getKey());
			dValue = dValue * 100;
			defaultDataSet.setValue(sKey, dValue);
		}
		
		return defaultDataSet;
	}

}
