package com.kz.fm_desktop;

import java.util.ArrayList;

public class MapperGetData {
	
	private ArrayList<String> aCategories;
	private double dTotalBalance;
	private double dMonthlyBalance;
	private double dMonthlyBudget;
	private double dMonthlyBudgetSetting;
	
	public ArrayList<String> getaCategories() {
		return aCategories;
	}
	public void setaCategories(ArrayList<String> aCategories) {
		this.aCategories = aCategories;
	}
	public double getdTotalBalance() {
		return dTotalBalance;
	}
	public void setdTotalBalance(double dTotalBalance) {
		this.dTotalBalance = dTotalBalance;
	}
	public double getdMonthlyBalance() {
		return dMonthlyBalance;
	}
	public void setdMonthlyBalance(double dMonthlyBalance) {
		this.dMonthlyBalance = dMonthlyBalance;
	}
	public double getdMonthlyBudget() {
		return dMonthlyBudget;
	}
	public void setdMonthlyBudget(double dMonthlyBudget) {
		this.dMonthlyBudget = dMonthlyBudget;
	}
	public double getdMonthlyBudgetSetting() {
		return dMonthlyBudgetSetting;
	}
	public void setdMonthlyBudgetSetting(double dMonthlyBudgetSetting) {
		this.dMonthlyBudgetSetting = dMonthlyBudgetSetting;
	}

}
