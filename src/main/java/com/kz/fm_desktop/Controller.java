package com.kz.fm_desktop;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.logging.Level;

import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.springframework.web.client.RestTemplate;

public class Controller {

	private static String sUrl;
	private static String sUrlSaveSettings;
	private static String sUrlCategories;
	private static String sUrlRefresh;
	private static String sUrlSaveTransaction;
	private static String sUrlGetPercentages;
	private static String sUrlGetMonthlyBudgetSetting;
	private static RestTemplate restTemplate = new RestTemplate();
	

	// Rounding format
	private static DecimalFormat roundingFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.US));

	static {
		try {
			PropertiesConfiguration config = new PropertiesConfiguration("application.properties");
			sUrl 						= config.getString("server.url");
			sUrlSaveSettings 			= config.getString("url.save.settings");
			sUrlCategories 				= config.getString("url.getCategories");
			sUrlRefresh 				= config.getString("url.refresh");
			sUrlSaveTransaction 		= config.getString("url.saveTransaction");
			sUrlGetPercentages 			= config.getString("url.getPercentages");
			sUrlGetMonthlyBudgetSetting = config.getString("url.getMonthlyBudgetSetting");
		} catch (ConfigurationException ce) {
			AppLogger.log(Level.SEVERE, ce.getMessage());
		}
	}

	
	public static ArrayList<String> getCategories() {
		MapperGetData result  = restTemplate.getForObject(sUrl + sUrlCategories, MapperGetData.class);
		return result.getaCategories();
	}
	
	public static void saveButtonAction() throws SQLException {
		MapperSendData mapperSend = new MapperSendData();
		
		String sType 	  = Gui.inputComboTransactionType.getSelectedItem().toString();
		String sValueText = Gui.inputTransactionValue.getText();
		String sCategory;

		if (Gui.inputComboCategory.isEnabled()) {
			sCategory = Gui.inputComboCategory.getSelectedItem().toString();
		} else {
			sCategory = null;
		}

		float fValue;

		try {
			fValue = Float.parseFloat(sValueText);
			String sDescription = Gui.inputDescription.getText();

			JFormattedTextField editor = Gui.inputDatePicker.getEditor();
			Date dateIDP = (Date) editor.getValue();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String sDate = format.format(dateIDP);
			AppLogger.log(Level.FINE, "Settings budget value: " + fValue);

			if (fValue < 0) {
				AppLogger.log(Level.INFO, "Input 'Value' must be a POSITIVE real number.");
				JOptionPane.showMessageDialog(null, "Input 'Value' must be a POSITIVE real number.");
			} else {
				mapperSend.setsType(sType);
				mapperSend.setsCategory(sCategory);
				mapperSend.setsDescription(sDescription);
				mapperSend.setsDate(sDate);
				
				if (sType.equals("Income")) {
					AppLogger.log(Level.FINE, "v1: " + fValue);
					mapperSend.setfValue(fValue);
				} else {
					AppLogger.log(Level.FINE, "v2: " + fValue);
					mapperSend.setfValue(-fValue);
				}
				
				restTemplate.postForObject(sUrl + sUrlSaveTransaction, mapperSend, String.class);
				
				// Set text in Gui for TotalBalance, MonthlyBalance, MoneyLeft
				refreshButtonAction();
			}
		
		} catch (Exception e) {
			AppLogger.log(Level.INFO, "Input 'Value' must be a real number");
			JOptionPane.showMessageDialog(null, "Input 'Value' must be a real number");
		}
	}

	public static void refreshButtonAction() throws SQLException {
		String sURL = sUrl + sUrlRefresh;
		MapperGetData result = restTemplate.getForObject(sURL, MapperGetData.class);

		Gui.textTotalBalance.setText(String.valueOf(roundingFormat.format(result.getdTotalBalance())));
		Gui.textMonthlyBalance.setText(String.valueOf(roundingFormat.format(result.getdMonthlyBalance())));
		Gui.textMoneyLeft.setText(String.valueOf(roundingFormat.format(result.getdMonthlyBudget())));
	}

	/*
	 * klik na gumb Save v Settings oknu poklice funkcijo iz ConnectionManager, ki
	 * iz podatkov, ki so v bazi, izracuna, koliko denarja je se ostalo za ta mesec
	 */
	public static void saveSettingsButtonAction() {
		MapperSendData mapperSend = new MapperSendData();

		try {
			double dBudget = Double.parseDouble(SettingsWindow.inputMonthlyGoal.getText());
			if (dBudget <= 0) {
				AppLogger.log(Level.INFO, "Input 'Monthly budget' must be a POSITIVE real number.");
				JOptionPane.showMessageDialog(null, "Input 'Monthly budget' must be a POSITIVE real number.");
			} else {
				// Save budget to db
				mapperSend.setdBudget(dBudget);
				restTemplate.postForObject(sUrl + sUrlSaveSettings, mapperSend, String.class);
				// Update Gui
				refreshButtonAction();
			}
		} catch (Exception e2) {
			AppLogger.log(Level.INFO, "Input 'Monthly budget' must be a real number");
			JOptionPane.showMessageDialog(null, "Input 'Monthly budget' must be a real number");
		}
	}

	// Set settings input fields to already defined values if they exist,
	// otherwise to some initial values.
	public static void openSettingsButtonAction() {
		MapperGetData result = restTemplate.getForObject(sUrl + sUrlGetMonthlyBudgetSetting, MapperGetData.class);
		String sMonthlyBudgetSetting = roundingFormat.format(result.getdMonthlyBudgetSetting());
		SettingsWindow.inputMonthlyGoal.setText(sMonthlyBudgetSetting);

	}
	
	public static HashMap<String, Double> getPercentages() {		
		HmMapper hmMapper = restTemplate.getForObject(sUrl + sUrlGetPercentages, HmMapper.class);
		return hmMapper.getHmPercentages();
	}

	public static void getMonthlyBudgetSetting() {
		MapperGetData result = restTemplate.getForObject(sUrl + sUrlGetMonthlyBudgetSetting, MapperGetData.class);
		SettingsWindow.inputMonthlyGoal.setText(String.valueOf(roundingFormat.format(result.getdMonthlyBudgetSetting())));
	}

}
