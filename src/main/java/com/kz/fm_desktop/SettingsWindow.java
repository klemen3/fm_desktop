package com.kz.fm_desktop;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

public class SettingsWindow {

	private static JFrame 	 settingsFrame;
	private static JPanel 	 settingsPanel;
	private static JLabel 	 labelMonthlyGoal;
	private static JButton 	 buttonSaveSettings;
	private static MigLayout settingsMig;
	
	static JTextField inputMonthlyGoal;
	
	
	public static void drawSettingsWindow() {
		settingsFrame 	   = new JFrame("Settings");
		settingsMig   	   = new MigLayout();
		settingsPanel 	   = new JPanel(settingsMig);
		
		inputMonthlyGoal   = new JTextField();
		labelMonthlyGoal   = new JLabel("Monthly budget:");
		buttonSaveSettings = new JButton("Save & Close");
		
		settingsPanel.add(labelMonthlyGoal);
		settingsPanel.add(inputMonthlyGoal);
		settingsPanel.add(buttonSaveSettings);
		
		buttonSaveSettings.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent e) {
				// vrednost budget dobimo iz inputMonthlyGoal in jo
				// posljemo Controllerju
				Controller.saveSettingsButtonAction();
				settingsFrame.dispatchEvent(new WindowEvent(settingsFrame, WindowEvent.WINDOW_CLOSING));
			}
		});
		
		
		inputMonthlyGoal.setPreferredSize(new Dimension(85, 23));
		
		settingsFrame.add(settingsPanel);
		
		// Frame settings
		settingsFrame.setResizable(false);
		settingsFrame.setLocationRelativeTo(null);
		settingsFrame.setVisible(true);
		settingsFrame.pack();
		settingsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}
	
}
