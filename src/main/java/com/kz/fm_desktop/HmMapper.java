package com.kz.fm_desktop;

import java.util.HashMap;

public class HmMapper {

	private HashMap<String, Double> hmPercentages;

	
	public HashMap<String, Double> getHmPercentages() {
		return hmPercentages;
	}

	public void setHmPercentages(HashMap<String, Double> hmPercentages) {
		this.hmPercentages = hmPercentages;
	}
	
	
}
