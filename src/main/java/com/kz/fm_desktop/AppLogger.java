package com.kz.fm_desktop;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;


public class AppLogger {

	private PropertiesConfiguration config;
	private String sApplicationLogPath;
	private String sLogLevel;
	private boolean bAppendLogToFile;
	
	private String consoleLogLevel;
	private Handler consoleHandler;
	
	static Logger LOGGER;
	public Handler fileHandler;
	
	
	private AppLogger() throws SecurityException, IOException {
		
		try {
			config = new PropertiesConfiguration("application.properties");
			
			sApplicationLogPath  = config.getString("log.path");
			sLogLevel 		    = config.getString("log.level");
			consoleLogLevel     = config.getString("console.log.level");
			bAppendLogToFile			= config.getBoolean("log.append");
			
			consoleHandler = new ConsoleHandler();
			consoleHandler.setLevel(Level.parse(consoleLogLevel));
			
			LOGGER = Logger.getLogger("Logger");
			fileHandler = new FileHandler(sApplicationLogPath, bAppendLogToFile);
			fileHandler.setFormatter(new SimpleFormatter());
			LOGGER.addHandler(fileHandler);
			LOGGER.setLevel(Level.parse(sLogLevel));
			LOGGER.addHandler(consoleHandler);
			LOGGER.setUseParentHandlers(false);
			
		} catch (ConfigurationException ce) {
			ce.printStackTrace();
		}
	}
	
	private static Logger getLogger() {
		if (LOGGER == null) {
			try {
				new AppLogger();
			} catch(IOException ioe) {
				ioe.printStackTrace();
			}
		}
		return LOGGER;
	}
	
	public static void log(Level level, String sMessage) {
		getLogger().log(level, sMessage);
	}


	
}
