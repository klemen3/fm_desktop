package com.kz.fm_desktop;

import java.sql.SQLException;
import java.util.logging.Level;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;



public class FMRunner {

	public static void main(String[] args) throws SQLException {
		
		// Default format
		String sLoggingFormat = "%1$tF %1$tT %4$s %5$s%6$s%n";
		
		PropertiesConfiguration config = null;
		try {
			config = new PropertiesConfiguration("application.properties");
		} catch (ConfigurationException ce) {
		}
		sLoggingFormat = config.getString("log.format");
		
		System.setProperty("java.util.logging.SimpleFormatter.format", sLoggingFormat);
		
		AppLogger.log(Level.INFO, "Starting...");
		
		Gui gui = new Gui();
		gui.drawGui();
	}
	
}

