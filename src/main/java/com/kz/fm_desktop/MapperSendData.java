package com.kz.fm_desktop;

public class MapperSendData {
	
	private String sType;
	private float fValue;
	private String sCategory;
	private String sDescription;
	private String sDate;
	private double dBudget;
	
	public String getsType() {
		return sType;
	}
	public void setsType(String sType) {
		this.sType = sType;
	}
	public float getfValue() {
		return fValue;
	}
	public void setfValue(float fValue) {
		this.fValue = fValue;
	}
	public String getsCategory() {
		return sCategory;
	}
	public void setsCategory(String sCategory) {
		this.sCategory = sCategory;
	}
	public String getsDescription() {
		return sDescription;
	}
	public void setsDescription(String sDescription) {
		this.sDescription = sDescription;
	}
	public String getsDate() {
		return sDate;
	}
	public void setsDate(String sDate) {
		this.sDate = sDate;
	}
	public double getdBudget() {
		return dBudget;
	}
	public void setdBudget(double dBudget) {
		this.dBudget = dBudget;
	}
	
	
	
	
}
